import container from "./inversify.config";
import createSqlWasm, { SQLWasm } from "sql-wasm";
import { DatabaseProvider } from "./app/provider/DatabaseProvider";
import { ImporterUI } from "./app/ui/ImporterUI";
import { ImporterController } from "./app/ui/ImporterController";
import { SettingsUI } from "./app/ui/SettingsUI";
import { SettingsController } from "./app/ui/SettingsController";
import { DATABASE_LOADED_HOOK } from "./app/Constants";
import { Settings } from "./app/Settings";
import { SettingsProvider } from "./app/provider/SettingsProvider";

Hooks.once('ready', () => {
    game.settings.register('pf1-psrd-importer', 'settings', {
        name: "PF1 PSRD Importer Settings",
        scope: "client",
        default: Settings.default(),
        type: Object,
        config: false,
        onChange: settings => {
            //
        }
    }
    );
});

Hooks.on("renderSettings", init);

async function init(app, html) {
    console.log("PF1-PSRD-Importer | initializing ...");

    let sqlWasm: SQLWasm = await createSqlWasm({ wasmUrl: "/modules/pf1-psrd-importer/scripts/sqlite3.wasm" });
    let databaseProvider = container.get<DatabaseProvider>(DatabaseProvider);
    databaseProvider.setSqlWasm(sqlWasm);

    let settings: Settings = container.get<SettingsProvider>(SettingsProvider).get();

    if (settings.databasePath) {
        databaseProvider.loadDatabase(settings.databasePath);
    }

    $('#settings').append(
        $(
            `<div class="game-system">
                <h2>PSRD Importer</h2>
            </div>
            <button id="pf1-psrd-importer-settings-button">
                <i class="fas fa-cogs"></i> Settings
            </button>
            <button id="pf1-psrd-importer-button" disabled>
                <i class="fas fa-file-import"></i> Import Data
            </button>
            `
        )
    )

    $('#pf1-psrd-importer-settings-button').on('click', () => {
        let controller = container.get(SettingsController);
        new SettingsUI().setController(controller).render(true);
    })

    $('#pf1-psrd-importer-button').on('click', () => {
        let controller = container.get(ImporterController);
        new ImporterUI().setController(controller).render(true);
    })

    Hooks.on(DATABASE_LOADED_HOOK, (databasePath) => {
        $('#pf1-psrd-importer-button').removeAttr('disabled');
        ui.notifications.info('PF1-PSRD-Importer | Database from ' + databasePath + " loaded!");
    })

    console.log("PF1-PSRD-Importer |  initalized.");
}