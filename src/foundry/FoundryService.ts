import { ActorType, Npc } from "./Actor";
import { injectable } from "inversify";
import { ItemType, Class } from "./Item";

@injectable()
export class FoundryService {

    public async createNewNpc(name: string, folder: any): Promise<Npc> {
        return Actor.create({ name: name, type: ActorType.NPC, folder: folder });
    }

    public async createNewItemFolder(name: string): Promise<any> {
        return Folder.create({ name: name, parent: null, type: "Item" });
    }

    public async createNewClass(name: string, folder: any): Promise<Class> {
        return Item.create({ name: name, type: ItemType.Class, folder: folder });
    }

    public async createNewTemporaryClass(name: string): Promise<Class> {
        return Item.create({ name: name, type: ItemType.Class }, { temporary: true });
    }



}