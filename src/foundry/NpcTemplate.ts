export interface NpcTemplate {
    abilities: Abilities;
    resources: CustomSkills;
    attributes: Attributes;
    details: Details;
    skills: { [key: string]: Skill };
    customSkills: CustomSkills;
    traits: Traits;
    currency: Currency;
    altCurrency: Currency;
}

export interface Abilities {
    str: Ability;
    dex: Ability;
    con: Ability;
    int: Ability;
    wis: Ability;
    cha: Ability;
}

export interface Ability {
    total: number;
    mod: number;
    value: number;
    checkMod: number;
    damage: number;
    drain: number;
    penalty: number;
    userPenalty: number;
    carryBonus?: number;
    carryMultiplier?: number;
}

export interface Currency {
    pp: number;
    gp: number;
    sp: number;
    cp: number;
}

export interface Attributes {
    creatureType: string;
    encumbrance: Encumbrance;
    vision: Vision;
    hd: HD;
    naturalAC: number;
    ac: AC;
    bab: Bab;
    cmd: Cmd;
    cmb: Bab;
    sr: Sr;
    cmbNotes: string;
    saveNotes: string;
    acNotes: string;
    cmdNotes: string;
    srNotes: string;
    attack: Attack;
    damage: Damage;
    maxDexBonus: null;
    acp: ACP;
    energyDrain: number;
    quadruped: boolean;
    savingThrows: SavingThrows;
    hp: HP;
    init: Init;
    prof: number;
    speed: Speed;
    conditions: { [key: string]: boolean };
    spellLevel: number;
    spells: Spells;
    spellcasting: Spellcasting;
    spelldc: Spellcasting;
}

export interface AC {
    normal: Bab;
    touch: Bab;
    flatFooted: Bab;
}

export interface Bab {
    value: number;
    total: number;
}

export interface ACP {
    gear: number;
    encumbrance: number;
    total: number;
}

export interface Attack {
    general: number;
    melee: number;
    ranged: number;
}

export interface Cmd {
    value: number;
    total: number;
    flatFootedTotal: number;
}

export interface Damage {
    general: number;
    weapon: number;
    spell: number;
}

export interface Encumbrance {
    level: number;
    levels: Levels;
    carriedWeight: number;
}

export interface Levels {
    light: number;
    medium: number;
    heavy: number;
    carry: number;
    drag: number;
}

export interface HD {
    base: Base;
    total: number;
    max: Base;
}

export interface Base {
    _deprecated: boolean;
    value: number;
}

export interface HP {
    value: number;
    min: number;
    base: number;
    max: number;
    temp: null;
    nonlethal: null;
}

export interface Init {
    value: number;
    bonus: number;
    total: number;
}

export interface SavingThrows {
    fort: Fort;
    ref: Fort;
    will: Fort;
}

export interface Fort {
    total: number;
}

export interface Speed {
    land: Burrow;
    climb: Burrow;
    swim: Burrow;
    burrow: Burrow;
    fly: Fly;
}

export interface Burrow {
    base: number;
    total: number | null;
}

export interface Fly {
    base: number;
    total: null;
    maneuverability: string;
}

export interface Spellcasting {
    _deprecated: boolean;
}

export interface Spells {
    concentration: Concentration;
    spellbooks: Spellbooks;
}

export interface Concentration {
    bonus: number;
    context: string;
}

export interface Spellbooks {
    primary: Spellbook;
    secondary: Spellbook;
    tertiary: Spellbook;
    spelllike: Spellbook;
}

export interface Spellbook {
    name: string;
    class: string;
    cl: Cl;
    concentration: number;
    concentrationFormula: string;
    concentrationNotes: string;
    clNotes: string;
    ability: AbilityType;
    autoSpellLevels: boolean;
    arcaneSpellFailure: boolean;
}

export enum AbilityType {
    Str = "str",
    Dex = "dex",
    Con = "con",
    Int = "int",
    Wis = "wis",
    Cha = "cha",
}

export interface Cl {
    base: number;
    value: number;
    total: number;
    formula: string;
}

export interface Sr {
    formula: string;
    total: number;
}

export interface Vision {
    lowLight: boolean;
}

export interface CustomSkills {
}

export interface Details {
    level: Level;
    alignment: string;
    biography: Biography;
    notes: Biography;
    bonusSkillRankFormula: string;
    type: string;
    environment: string;
    xp: Xp;
    cr: string;
    source: string;
}

export interface Xp {
    value: number;
}


export interface Biography {
    value: string;
    public: string;
}

export interface Level {
    value: number;
    min: number;
    max: number;
}

export interface Skill {
    value: number;
    ability: AbilityType;
    rt: boolean;
    acp: boolean;
    rank: number;
    notes: string;
    mod: number;
    background: boolean;
}

export interface Traits {
    size: string;
    senses: string;
    dr: string;
    eres: string;
    cres: string;
    languages: TraitValue;
    di: TraitValue;
    dv: TraitValue;
    ci: TraitValue;
    perception: Spellcasting;
}

export interface TraitValue {
    value: any[];
    custom: string;
}


