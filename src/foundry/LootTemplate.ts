import { ItemDescription, PhysicalItem } from "./ItemTemplates";

export interface LootTemplate extends ItemDescription, PhysicalItem {
    subType: string;
}