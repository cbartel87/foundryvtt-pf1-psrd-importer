export interface Changes {
    changes: any[];
    changeFlags: ChangeFlags;
}

export interface ChangeFlags {
    loseDexToAC: boolean;
    noStr: boolean;
    noDex: boolean;
    oneInt: boolean;
    oneWis: boolean;
    oneCha: boolean;
    noEncumbrance: boolean;
    mediumArmorFullSpeed: boolean;
    heavyArmorFullSpeed: boolean
}

export interface ContextNotes {
    contextNotes: any[]
}

export interface ItemDescription {
    description: Description;
    source: string
}

export interface Description {
    value: string;
    chat: string;
    unidentified: string;
}

export interface PhysicalItem {
    quantity: number;
    weight: number;
    price: number;
    identified: boolean;
    hp: HP;
    hardness: number;
    carried: boolean;
}

export interface HP {
    max: number;
    value: number;
}

export interface ActivatedEffect {
    activation: Activation;
    duration: Duration;
    target: Target;
    range: Duration;
    uses: Uses;
}

export interface Activation {
    cost: number;
    type: string;
}

export interface Duration {
    value: null;
    units: string;
}

export interface Target {
    value: string;
}

export interface Uses {
    value: number;
    max: number;
    per: null;
}

export interface Action {
    measureTemplate: MeasureTemplate;
    actionType: null;
    attackBonus: string;
    critConfirmBonus: string;
    damage: Damage;
    attackParts: any[];
    formula: string;
    ability: Ability;
    save: Save;
    effectNotes: string;
    attackNotes: string;
}

export interface Ability {
    attack: null;
    damage: null;
    damageMult: number;
    critRange: number;
    critMult: number;
}

export interface Damage {
    parts: any[];
}

export interface MeasureTemplate {
    type: string;
    size: number;
}

export interface Save {
    dc: number;
    description: string;
}
