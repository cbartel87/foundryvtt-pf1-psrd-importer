import { ItemDescription, PhysicalItem, Changes, ContextNotes } from "./ItemTemplates";

export interface EquipmentTemplate extends ItemDescription, PhysicalItem, Changes, ContextNotes {
    equipped: boolean;
    armor: Armor;
    armorType: ArmorType;
    spellFailure: number;
    slot: string;
}

export interface Armor {
    type: string;
    value: number;
    dex: null;
    acp: number;
    enh: number;
}

export interface ArmorType {
    value: string;
    _deprecated: boolean;
}
