import { Item, ItemData } from "./Item"
import { NpcTemplate } from "./NpcTemplate";

export interface Actor<T> {
    apps: any[];
    compendium: any;
    data: ActorData<T>;
    items: Item<any>[];
    options: any;
    update(data: ActorData<T>, options?: any);
    createOwnedItem(itemData: ItemData<any>, options?: any);
}

export interface ActorData<T> {
    data: T;
    flags: any;
    img: string;
    items: ItemData<any>[];
    name: string;
    permission: any;
    sort: number;
    token: any;
    type: ActorType;
    _id: string;
}

export enum ActorType {
    NPC = "npc",
    Character = "character"
}

export interface Npc extends Actor<NpcTemplate> { }