import { ItemDescription, ActivatedEffect, Action } from "./ItemTemplates";

export interface AttackTemplate extends ItemDescription, ActivatedEffect, Action {
    attackType: string;
    masterwork: boolean;
    enh: null;
    proficient: boolean;
    primaryAttack: boolean;
}