import { WeaponTemplate } from "./WeaponTemplate";
import { EquipmentTemplate } from "./EquipmentTemplate";
import { ConsumableTemplate } from "./ConsumableTemplate";
import { LootTemplate } from "./LootTemplate";
import { ClassTemplate } from "./ClassTemplate";
import { SpellTemplate } from "./SpellTemplate";
import { FeatTemplate } from "./FeatTemplate";
import { BuffTemplate } from "./BuffTemplate";
import { AttackTemplate } from "./AttackTemplate";

export interface Item<T> {
    apps: any;
    compendium: any;
    data: ItemData<T>;
    labels: { [key: string]: string };
    options: any;
    sheet: any;
    update(data: ItemData<T>, options?: any);
}

export interface ItemData<T> {
    data: T;
    flags: any;
    img: string;
    name: string;
    permission: any;
    sort: number;
    type: ItemType;
    _id: string;
}

export enum ItemType {
    Weapon = "weapon",
    Equipment = "equipment",
    Consumable = "consumable",
    Loot = "loot",
    Class = "class",
    Spell = "spell",
    Feat = "feat",
    Buff = "buff",
    Attack = "attack"
}

export interface Weapon extends Item<WeaponTemplate> { }
export interface Equipment extends Item<EquipmentTemplate> { }
export interface Consumable extends Item<ConsumableTemplate> { }
export interface Loot extends Item<LootTemplate> { }
export interface Class extends Item<ClassTemplate> { }
export interface Spell extends Item<SpellTemplate> { }
export interface Feat extends Item<FeatTemplate> { }
export interface Buff extends Item<BuffTemplate> { }
export interface Attack extends Item<AttackTemplate> { }