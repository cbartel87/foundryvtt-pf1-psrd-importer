import { ItemDescription, Changes, ContextNotes } from "./ItemTemplates";

export interface BuffTemplate extends ItemDescription, Changes, ContextNotes {
    buffType: string;
    active: boolean;
    level: number;
}