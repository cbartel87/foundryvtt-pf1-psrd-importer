import { ItemDescription, Changes } from "./ItemTemplates";

export interface ClassTemplate extends ItemDescription, Changes {
    classType: string;
    levels: number;
    hd: number;
    hp: number;
    bab: string;
    skillsPerLevel: number;
    creatureType: string;
    savingThrows: SavingThrows;
    fc: Fc;
    classSkills: ClassSkills;
}

export interface ClassSkills {
}

export interface Fc {
    hp: Alt;
    skill: Alt;
    alt: Alt;
}

export interface Alt {
    value: number;
}

export interface SavingThrows {
    fort: Fort;
    ref: Fort;
    will: Fort;
}

export interface Fort {
    value: string;
}