export class CallbackFilePicker extends FilePicker {

  callback: Function;

  setCallback(callback: Function): CallbackFilePicker {
    this.callback = callback;
    return this;
  }

  _onSubmit(ev) {
    super._onSubmit(ev);
    this.callback(ev);
  }
}