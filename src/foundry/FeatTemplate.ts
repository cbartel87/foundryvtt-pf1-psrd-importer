import { ItemDescription, ActivatedEffect, Action, Changes, ContextNotes } from "./ItemTemplates";

export interface FeatTemplate extends ItemDescription, ActivatedEffect, Action, Changes, ContextNotes {
    featType: string;
    requirements: string;
    recharge: Recharge;
    time: DamageType;
    damageType: DamageType;
}

export interface DamageType {
    value: string;
    _deprecated: boolean;
}

export interface Recharge {
    value: null;
    charged: boolean;
    _deprecated: boolean;
}
