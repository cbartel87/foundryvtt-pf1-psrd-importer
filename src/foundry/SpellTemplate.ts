import { ItemDescription, ActivatedEffect, Action } from "./ItemTemplates";

export interface SpellTemplate extends ItemDescription, ActivatedEffect, Action {
    learnedAt: LearnedAt;
    level: number;
    clOffset: number;
    slOffset: number;
    school: string;
    subschool: string;
    types: string;
    components: Components;
    castTime: string;
    materials: Materials;
    spellbook: string;
    preparation: Preparation;
    sr: boolean;
    shortDescription: string;
    spellDuration: string;
    spellEffect: string;
    spellArea: string;
    spellType: SpellTemplateValue;
    time: SpellTemplateValue;
    damageType: SpellTemplateValue;
    concentration: SpellTemplateValue;
    ritual: SpellTemplateValue;
    prepared: SpellTemplateValue;
}

export interface Components {
    value: string;
    verbal: boolean;
    somatic: boolean;
    material: boolean;
    focus: boolean;
    divineFocus: number;
}

export interface SpellTemplateValue {
    value: boolean | null | string;
    _deprecated: boolean;
}

export interface LearnedAt {
    class: any[];
    domain: any[];
    subDomain: any[];
    elementalSchool: any[];
    bloodline: any[];
}

export interface Materials {
    value: string;
    consumed: SpellTemplateValue;
    focus: string;
}

export interface Preparation {
    mode: string;
    preparedAmount: number;
    maxAmount: number;
}