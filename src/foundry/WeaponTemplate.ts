import { ItemDescription, PhysicalItem, Changes, ContextNotes } from "./ItemTemplates";

export interface WeaponTemplate extends ItemDescription, PhysicalItem, Changes, ContextNotes {
    equipped: boolean;
    masterwork: boolean;
    weaponType: string;
    properties: Properties;
    enh: null;
    weaponData: WeaponData;
    bonus: Bonus;
    damageType: Damage;
    damage2: Damage;
    damage2Type: Damage;
}

export interface Bonus {
    value: number;
    _deprecated: boolean;
}

export interface Damage {
    value: string;
    _deprecated: boolean;
}

export interface Properties {
}

export interface WeaponData {
    damageRoll: string;
    damageType: string;
    critRange: number;
    critMult: number;
    isMelee: boolean;
    range: null;
}