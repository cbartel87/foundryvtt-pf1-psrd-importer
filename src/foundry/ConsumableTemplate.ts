import { ItemDescription, PhysicalItem, ActivatedEffect, Action } from "./ItemTemplates";

export interface ConsumableTemplate extends ItemDescription, PhysicalItem, ActivatedEffect, Action {
    consumableType: string;
    uses: Uses;
    charges: Charges;
    consume: Consume;
    autoUse: Auto;
    autoDestroy: Auto;
}

export interface Auto {
    value: boolean;
    _deprecated: boolean;
}

export interface Charges {
    value: number;
    max: number;
    _deprecated: boolean;
}

export interface Consume {
    value: string;
    _deprecated: boolean;
}

export interface Uses {
    value: number;
    max: number;
    maxFormula: string;
    per: null;
}