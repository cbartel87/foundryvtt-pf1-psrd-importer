import "reflect-metadata";

import { Container } from "inversify";
import { DatabaseProvider } from "./app/provider/DatabaseProvider";
import { SectionsDAO } from "./app/dao/SectionsDAO";
import { FeatRepository } from "./app/repository/FeatRepository";
import { FeatTypesDAO } from "./app/dao/FeatTypesDAO";
import { SpellDetailsDAO } from "./app/dao/SpellDetailsDAO";
import { SectionRepository } from "./app/repository/SectionRepository";
import { SpellRepository } from "./app/repository/SpellRepository";
import { SkillAttributesDAO } from "./app/dao/SkillAttributesDAO";
import { SpellComponentsDAO } from "./app/dao/SpellComponentsDAO";
import { SpellDescriptorsDAO } from "./app/dao/SpellDescriptorsDAO";
import { SpellEffectsDAO } from "./app/dao/SpellEffectsDAO";
import { SpellListsDAO } from "./app/dao/SpellListsDAO";
import { SpellSubschoolsDAO } from "./app/dao/SpellSubschoolsDAO";
import { ItemDetailsDAO } from "./app/dao/ItemDetailsDAO";
import { ItemMiscDAO } from "./app/dao/ItemMiscDAO";
import { CreatureDetailsDAO } from "./app/dao/CreatureDetailsDAO";
import { CreatureSpellsDAO } from "./app/dao/CreatureSpellsDAO";
import { AbilityTypesDAO } from "./app/dao/AbilityTypesDAO";
import { AfflictionDetailsDAO } from "./app/dao/AfflictionDetailsDAO";
import { AnimalCompanionDetailsDAO } from "./app/dao/AnimalCompanionDetailsDAO";
import { ArmyDetailsDAO } from "./app/dao/ArmyDetailsDAO";
import { ClassDetailsDAO } from "./app/dao/ClassDetailsDAO";
import { FeatTypeDescriptionsDAO } from "./app/dao/FeatTypeDescriptionsDAO";
import { HauntDetailsDAO } from "./app/dao/HauntDetailsDAO";
import { KingdomResourceDetailsDAO } from "./app/dao/KingdomResourceDetailsDAO";
import { LinkDetailsDAO } from "./app/dao/LinkDetailsDAO";
import { MythicSpellDetailsDAO } from "./app/dao/MythicSpellDetailsDAO";
import { ResourceDetailsDAO } from "./app/dao/ResourceDetailsDAO";
import { SettlementDetailsDAO } from "./app/dao/SettlementDetailsDAO";
import { TrapDetailsDAO } from "./app/dao/TrapDetailsDAO";
import { UrlReferencesDAO } from "./app/dao/UrlReferencesDAO";
import { VehicleDetailsDAO } from "./app/dao/VehicleDetailsDAO";
import { ImporterController } from "./app/ui/ImporterController";
import { ActorService } from "./app/service/ActorService";
import { FoundryService } from "./foundry/FoundryService";
import { CreatureRepository } from "./app/repository/CreatureRepository";
import { NpcMapper } from "./app/mapper/NpcMapper";
import { ClassService } from "./app/service/ClassService";
import { ClassMapper } from "./app/mapper/ClassMapper";
import { SettingsController } from "./app/ui/SettingsController";
import { FeatService } from "./app/service/FeatService";
import { SettingsProvider } from "./app/provider/SettingsProvider";
import { ItemService } from "./app/service/ItemService";


var container = new Container();

//General
container.bind<DatabaseProvider>(DatabaseProvider).to(DatabaseProvider).inSingletonScope();
container.bind<SettingsProvider>(SettingsProvider).to(SettingsProvider).inSingletonScope();
container.bind<SettingsController>(SettingsController).to(SettingsController);
container.bind<ImporterController>(ImporterController).to(ImporterController);

//Mapper
container.bind<ClassMapper>(ClassMapper).to(ClassMapper);
container.bind<NpcMapper>(NpcMapper).to(NpcMapper);

//Service
container.bind<ActorService>(ActorService).to(ActorService);
container.bind<ClassService>(ClassService).to(ClassService);
container.bind<FeatService>(FeatService).to(FeatService);
container.bind<FoundryService>(FoundryService).to(FoundryService);
container.bind<ItemService>(ItemService).to(ItemService);

//Repository
container.bind<SectionRepository>(SectionRepository).to(SectionRepository);
container.bind<FeatRepository>(FeatRepository).to(FeatRepository);
container.bind<SpellRepository>(SpellRepository).to(SpellRepository);
container.bind<CreatureRepository>(CreatureRepository).to(CreatureRepository);

//DAO
container.bind<AbilityTypesDAO>(AbilityTypesDAO).to(AbilityTypesDAO);
container.bind<AfflictionDetailsDAO>(AfflictionDetailsDAO).to(AfflictionDetailsDAO);
container.bind<AnimalCompanionDetailsDAO>(AnimalCompanionDetailsDAO).to(AnimalCompanionDetailsDAO);
container.bind<ArmyDetailsDAO>(ArmyDetailsDAO).to(ArmyDetailsDAO);
container.bind<ClassDetailsDAO>(ClassDetailsDAO).to(ClassDetailsDAO);
container.bind<CreatureDetailsDAO>(CreatureDetailsDAO).to(CreatureDetailsDAO);
container.bind<CreatureSpellsDAO>(CreatureSpellsDAO).to(CreatureSpellsDAO);
container.bind<FeatTypeDescriptionsDAO>(FeatTypeDescriptionsDAO).to(FeatTypeDescriptionsDAO);
container.bind<FeatTypesDAO>(FeatTypesDAO).to(FeatTypesDAO);
container.bind<HauntDetailsDAO>(HauntDetailsDAO).to(HauntDetailsDAO);
container.bind<ItemDetailsDAO>(ItemDetailsDAO).to(ItemDetailsDAO);
container.bind<ItemMiscDAO>(ItemMiscDAO).to(ItemMiscDAO);
container.bind<KingdomResourceDetailsDAO>(KingdomResourceDetailsDAO).to(KingdomResourceDetailsDAO);
container.bind<LinkDetailsDAO>(LinkDetailsDAO).to(LinkDetailsDAO);
container.bind<MythicSpellDetailsDAO>(MythicSpellDetailsDAO).to(MythicSpellDetailsDAO);
container.bind<ResourceDetailsDAO>(ResourceDetailsDAO).to(ResourceDetailsDAO);
container.bind<SectionsDAO>(SectionsDAO).to(SectionsDAO);
container.bind<SettlementDetailsDAO>(SettlementDetailsDAO).to(SettlementDetailsDAO);
container.bind<SkillAttributesDAO>(SkillAttributesDAO).to(SkillAttributesDAO);
container.bind<SpellComponentsDAO>(SpellComponentsDAO).to(SpellComponentsDAO);
container.bind<SpellDescriptorsDAO>(SpellDescriptorsDAO).to(SpellDescriptorsDAO);
container.bind<SpellDetailsDAO>(SpellDetailsDAO).to(SpellDetailsDAO);
container.bind<SpellEffectsDAO>(SpellEffectsDAO).to(SpellEffectsDAO);
container.bind<SpellListsDAO>(SpellListsDAO).to(SpellListsDAO);
container.bind<SpellSubschoolsDAO>(SpellSubschoolsDAO).to(SpellSubschoolsDAO);
container.bind<TrapDetailsDAO>(TrapDetailsDAO).to(TrapDetailsDAO);
container.bind<UrlReferencesDAO>(UrlReferencesDAO).to(UrlReferencesDAO);
container.bind<VehicleDetailsDAO>(VehicleDetailsDAO).to(VehicleDetailsDAO);

export default container;