import { SettingsController } from "./SettingsController";
import { CallbackFilePicker } from "../../foundry/CallbackFilePicker";

export class SettingsUI extends Application {

    private controller: SettingsController;

    public setController(controller: SettingsController): SettingsUI {
        this.controller = controller;
        return this;
    }

    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            title: 'PF1 PSRD Importer - Settings',
            id: "pf1-psrd-importer-settings",
            template: "modules/pf1-psrd-importer/templates/settings.html",
            width: 600,
            height: "auto",
            closeOnSubmit: true
        })
    }

    activateListeners(html) {
        super.activateListeners(html);
        $('#pf1-psrd-importer-database-filepicker').on('click', () => {
            new CallbackFilePicker().setCallback((event) => {
                $('#pf1-psrd-importer-database-path').val(event.target.file.value);
            }).browse();
        });

        $(`#pf1-psrd-importer-packs-classes`).val(this.controller.getClassesPack());
        $(`#pf1-psrd-importer-packs-racialhd`).val(this.controller.getRacialHdPack());
        $(`#pf1-psrd-importer-packs-feats`).val(this.controller.getFeatsPack());
        $(`#pf1-psrd-importer-packs-items`).val(this.controller.getItemsPack());


        $('#pf1-psrd-importer-settings-submit').on('click', () => {
            let databasePath = <string>$('#pf1-psrd-importer-database-path').val();
            if (databasePath != '' || databasePath) {
                this.controller.loadDatabase(databasePath);
            }

            let classesPack = $('#pf1-psrd-importer-packs-classes option:selected').text();
            this.controller.setClassesPack(classesPack);

            let racialHdPack = $('#pf1-psrd-importer-packs-racialhd option:selected').text();
            this.controller.setRacialHdPack(racialHdPack);

            let featsPack = $('#pf1-psrd-importer-packs-feats option:selected').text();
            this.controller.setFeatsPack(featsPack);


            let itemsPack = $('#pf1-psrd-importer-packs-items option:selected').text();
            this.controller.setItemsPack(itemsPack);

            this.close();
        });

        $('#pf1-psrd-importer-settings-cancel').on('click', () => this.close());

    }

    getData(options) {
        return {
            settings: game.settings.get('pf1-psrd-importer', 'settings'),
            packs: this.controller.getPacks()
        }
    }

}