import { ImporterController } from "./ImporterController";
import { Creature } from "../model/Creature";

export class ImporterUI extends Application {

    private controller: ImporterController;

    public setController(controller: ImporterController): ImporterUI {
        this.controller = controller;
        return this;
    }

    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            title: 'PF1 PSRD Importer',
            id: "pf1-psrd-importer",
            template: "modules/pf1-psrd-importer/templates/importer.html",
            width: 600,
            height: 600,
            resizable: true,
            closeOnSubmit: true
        })
    }

    activateListeners(html) {
        super.activateListeners(html);
        html = html[0];
        const tabs = new TabsV2({ navSelector: "#pf1-psrd-importer-nav", contentSelector: "#pf1-psrd-importer-content", initial: "npc", callback: () => { } });
        tabs.bind(html);


        let npcClassList = $('#pf1-psrd-importer-npc-class-list');
        let data = this.controller.getNpcData();
        for (let idx = 0; idx < data.length; idx++) {
            this.generateNpcSection(npcClassList, data[idx], idx);
        }
    }

    private generateNpcSection(npcClassList: JQuery<HTMLElement>, npcData: Creature, id: number) {
        npcClassList.append(`
            <div class="form-group submenu" style="border: 1px thin">
                <label><b>${npcData.name}</b></label>
                <button type="button" id="npc${id}">generate</button>
                <p class="notes" id="npc${id}notes">${npcData.description}</p>
            </div>
            <hr>
        `)
        $(`#npc${id}`).on('click', () => {
            let folderName = <string>$('#pf1-psrd-importer-actor-folder-name').val();
            this.controller.generateNPC(npcData, folderName).then(result => {
                let msg: string;
                if (result.success) {
                    msg = `Imported ${npcData.name} !`;
                } else {
                    msg = `${npcData.name} was not successfully imported!<br /> Errors: <br/>`
                    for (let errorMessage of result.messages) {
                        msg += `${errorMessage}<br />`;
                    }
                    $(`#npc${id}notes`).css('color', 'red');
                }
                $(`#npc${id}notes`).html(msg);
                $(`#npc${id}`).off('click').on('click', () => {
                    result.data.sheet.render(true);
                });
                $(`#npc${id}`).html('show sheet');
            });
        });
    }

    getData(options) {
        return {
            actorFolders: this.controller.getActorFolders()
        }
    }


}