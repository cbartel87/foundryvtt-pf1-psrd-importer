import { injectable, inject } from "inversify";
import { DatabaseProvider } from "../provider/DatabaseProvider";
import { SettingsProvider } from "../provider/SettingsProvider";

@injectable()
export class SettingsController {


    @inject(DatabaseProvider)
    private databaseProvider: DatabaseProvider;

    @inject(SettingsProvider)
    private settingsProvider: SettingsProvider;


    public loadDatabase(path: string): void {
        let settings = this.settingsProvider.get();

        if (settings.databasePath == path) {
            return;
        }

        settings.databasePath = path;
        this.settingsProvider.set(settings);

        this.databaseProvider.loadDatabase(path);
    }

    public getPacks(): Array<string> {
        return game.packs.map(p => p.metadata.name);
    }

    public getClassesPack(): string {
        return this.settingsProvider.get().classesPack;
    }

    public getRacialHdPack(): string {
        return this.settingsProvider.get().racialhdPack;
    }

    public getFeatsPack(): string {
        return this.settingsProvider.get().featsPack;
    }

    public getItemsPack(): string {
        return this.settingsProvider.get().itemsPack;
    }

    public setClassesPack(value: string) {
        let settings = this.settingsProvider.get();
        if (settings.classesPack == value) {
            return;
        }
        settings.classesPack = value;
        this.settingsProvider.set(settings);
    }

    public setRacialHdPack(value: string) {
        let settings = this.settingsProvider.get();
        if (settings.racialhdPack == value) {
            return;
        }
        settings.racialhdPack = value;
        this.settingsProvider.set(settings);
    }

    public setFeatsPack(value: string) {
        let settings = this.settingsProvider.get();
        if (settings.featsPack == value) {
            return;
        }
        settings.featsPack = value;
        this.settingsProvider.set(settings);
    }


    public setItemsPack(value: string) {
        let settings = this.settingsProvider.get();
        if (settings.itemsPack == value) {
            return;
        }
        settings.itemsPack = value;
        this.settingsProvider.set(settings);
    }

}