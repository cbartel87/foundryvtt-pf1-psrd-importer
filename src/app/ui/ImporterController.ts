import { injectable, inject } from "inversify";
import { ActorService } from "../service/ActorService";
import { Creature } from "../model/Creature";
import { ClassService } from "../service/ClassService";
import { FeatService } from "../service/FeatService";
import { SettingsProvider } from "../provider/SettingsProvider";
import { ItemService } from "../service/ItemService";

@injectable()
export class ImporterController {


    @inject(ActorService)
    private actorService: ActorService;

    @inject(ClassService)
    private classService: ClassService;

    @inject(FeatService)
    private featService: FeatService;

    @inject(ItemService)
    private itemService: ItemService;

    public getActorFolders(): Array<string> {
        return game.actors.directory.folders.map(f => f.data.name);
    }

    public getNpcData(): Array<Creature> {
        return this.actorService.getAllCreatures();
    }

    public async generateNPC(creature: Creature, folderName: string): Promise<GenerationResult> {
        let actorFolder = null;
        if (folderName && folderName != '') {
            for (let folder of game.actors.directory.folders) {
                if (folder.data.name == folderName) {
                    actorFolder = folder;
                }
            }
        }

        let npc = await this.actorService.createNpcFromCreature(creature, actorFolder);
        let success = true;
        let messages = new Array<string>();

        let racialHD = await this.classService.getRacialHDForCreature(creature);
        if (racialHD == null) {
            messages.push(`RacialHD ${creature.creatureType} not found!`);
            success = false;
        } else {
            npc.createOwnedItem(racialHD.data);
        }

        if (creature.level) {
            let creatureClass = await this.classService.getClassForCreature(creature);
            if (creatureClass == null) {
                messages.push(`Class ${creature.level} not found!`);
                success = false;
            } else {
                npc.createOwnedItem(creatureClass.data);
            }
        }

        let npcFeatResult = await this.featService.getFeatsForCreature(creature);
        if (npcFeatResult.featsNotFound.length > 0) {
            success = false;
            for (let featName of npcFeatResult.featsNotFound) {
                messages.push(`Feat ${featName} not found!`);
            }
        }
        for (let feat of npcFeatResult.feats) {
            npc.createOwnedItem(feat.data);
        }

        let itemResult = await this.itemService.getItemsForCreature(creature);
        if (itemResult.itemsNotFound.length > 0) {
            success = false;
            for (let itemName of itemResult.itemsNotFound) {
                messages.push(`Item ${itemName} not found!`);
            }
        }
        for (let item of itemResult.items) {
            npc.createOwnedItem(item.data);
        }

        return { success: success, messages: messages, data: npc }
    }




}

export interface GenerationResult {
    success: boolean;
    messages: Array<string>;
    data: any;
}
