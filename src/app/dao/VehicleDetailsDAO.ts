import { AbstractDAO } from "./AbstractDAO";
import { injectable } from "inversify";

@injectable()
export class VehicleDetailsDAO extends AbstractDAO<VehicleDetailsEntity> {

}

export interface VehicleDetailsEntity {
    vehicle_details_id: number;
    section_id: number;
    size: string;
    vehicle_type: string;
    squares: string;
    cost: string;
    ac: string;
    hardness: string;
    hp: string;
    base_save: string;
    maximum_speed: string;
    acceleration: string;
    cmb: string;
    cmd: string;
    ramming_damage: string;
    propulsion: string;
    driving_check: string;
    forward_facing: string;
    driving_device: string;
    driving_space: string;
    decks: string;
    deck: string;
    weapons: string;
    crew: string;
    passengers: string;
}