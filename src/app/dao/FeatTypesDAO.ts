import { AbstractDAO } from "./AbstractDAO";
import { injectable } from "inversify";

@injectable()
export class FeatTypesDAO extends AbstractDAO<FeatTypesEntity> {

    public findBySectionId(sectionId: number): Array<FeatTypesEntity> {
        return this.select(`
            SELECT
                feat_type_id,
                section_id,
                feat_type
            FROM
                feat_types
            WHERE
                section_id = :sectionId
        `, { ':sectionId': sectionId }
        );
    }

}

export interface FeatTypesEntity {
    feat_type_id: number;
    section_id: number;
    feat_type: string;
}