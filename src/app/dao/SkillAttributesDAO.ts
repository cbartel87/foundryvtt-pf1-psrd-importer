import { AbstractDAO } from "./AbstractDAO";
import { injectable } from "inversify";

@injectable()
export class SkillAttributesDAO extends AbstractDAO<SkillAttributesEntity> {

    public findBySectionId(sectionId: number): SkillAttributesEntity {
        return this.singleSelect(`
            SELECT
                skill_attribute_id,
                section_id,
                attribute,
                armor_check_penalty,
                trained_only
            FROM
                skill_attributes    
            WHERE
                section_id = :sectionId
        `, { ':sectionId': sectionId }
        );
    }

}

export interface SkillAttributesEntity {
    skill_attribute_id: number;
    section_id: number;
    attribute: string;
    armor_check_penalty: number;
    trained_only: number;
}