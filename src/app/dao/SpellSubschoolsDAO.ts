import { AbstractDAO } from "./AbstractDAO";
import { injectable } from "inversify";

@injectable()
export class SpellSubschoolsDAO extends AbstractDAO<SpellSubschoolsEntity> {

    public findBySectionId(sectionId: number): Array<SpellSubschoolsEntity> {
        return this.select(`
            SELECT
                spell_subschool_id,
                section_id,
                subschool
            FROM
                spell_subschools        
            WHERE
                section_id = :sectionId
        `, { ':sectionId': sectionId }
        );
    }

}

export interface SpellSubschoolsEntity {
    spell_subschool_id: number;
    section_id: number;
    subschool: string;
}