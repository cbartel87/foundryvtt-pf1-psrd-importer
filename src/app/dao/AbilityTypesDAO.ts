import { AbstractDAO } from "./AbstractDAO";
import { injectable } from "inversify";

@injectable()
export class AbilityTypesDAO extends AbstractDAO<AbilityTypesEntity> {

    public findBySectionId(sectionId: number): Array<AbilityTypesEntity> {
        return this.select(`
            SELECT
                ability_type_id,
                section_id,
                ability_type
            FROM
                ability_types
            WHERE
                section_id = :sectionId
        `, { ':sectionId': sectionId }
        );
    }

}

export interface AbilityTypesEntity {
    ability_type_id: number;
    section_id: number;
    ability_type: string;
}