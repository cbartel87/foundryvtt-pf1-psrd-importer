import { AbstractDAO } from "./AbstractDAO";
import { injectable } from "inversify";

@injectable()
export class ArmyDetailsDAO extends AbstractDAO<ArmyDetailsEntity> {

}

export interface ArmyDetailsEntity {
    army_detail_id: number;
    section_id: number;
    xp: string;
    creature_type: string;
    alignment: string;
    size: string;
    hp: string;
    acr: string;
    dv: string;
    om: string;
    special: string;
    speed: string;
    consumption: string;
    tactics: string;
    resources: string;
    note: string;
}