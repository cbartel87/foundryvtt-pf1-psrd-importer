import { AbstractDAO } from "./AbstractDAO";
import { injectable } from "inversify";

@injectable()
export class FeatTypeDescriptionsDAO extends AbstractDAO<FeatTypeDescriptionsEntity> {

}

export interface FeatTypeDescriptionsEntity {
    feat_type_id: number;
    section_id: number;
    feat_type_description: string;
}