import { AbstractDAO } from "./AbstractDAO";
import { injectable } from "inversify";

@injectable()
export class CreatureDetailsDAO extends AbstractDAO<CreatureDetailsEntity> {

    public findBySectionId(sectionId: number): CreatureDetailsEntity {
        return this.singleSelect(`
            SELECT
                creature_details_id,
                section_id,
                sex,
                super_race,
                level,
                cr,
                mr,
                xp,
                alignment,
                size,
                creature_type,
                creature_subtype,
                init,
                senses,
                aura,
                ac,
                hp,
                fortitude,
                reflex,
                will,
                defensive_abilities,
                dr,
                resist,
                immune,
                sr,
                weaknesses,
                speed,
                melee,
                ranged,
                space,
                reach,
                special_attacks,
                strength,
                dexterity,
                constitution,
                intelligence,
                wisdom,
                charisma,
                base_attack,
                cmb,
                cmd,
                feats,
                skills,
                racial_modifiers,
                languages,
                special_qualities,
                gear,
                combat_gear,
                other_gear,
                boon,
                environment,
                organization,
                treasure,
                hit_dice,
                natural_armor,
                breath_weapon
            FROM
                creature_details 
            WHERE
                section_id = :sectionId
        `, { ':sectionId': sectionId }
        );
    }

}

export interface CreatureDetailsEntity {
    creature_details_id: number;
    section_id: number;
    sex: string;
    super_race: string;
    level: string;
    cr: string;
    mr: string;
    xp: string;
    alignment: string;
    size: string;
    creature_type: string;
    creature_subtype: string;
    init: string;
    senses: string;
    aura: string;
    ac: string;
    hp: string;
    fortitude: string;
    reflex: string;
    will: string;
    defensive_abilities: string;
    dr: string;
    resist: string;
    immune: string;
    sr: string;
    weaknesses: string;
    speed: string;
    melee: string;
    ranged: string;
    space: string;
    reach: string;
    special_attacks: string;
    strength: string;
    dexterity: string;
    constitution: string;
    intelligence: string;
    wisdom: string;
    charisma: string;
    base_attack: string;
    cmb: string;
    cmd: string;
    feats: string;
    skills: string;
    racial_modifiers: string;
    languages: string;
    special_qualities: string;
    gear: string;
    combat_gear: string;
    other_gear: string;
    boon: string;
    environment: string;
    organization: string;
    treasure: string;
    hit_dice: string;
    natural_armor: string;
    breath_weapon: string;
}