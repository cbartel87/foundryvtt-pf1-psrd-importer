import { AbstractDAO } from "./AbstractDAO";
import { injectable } from "inversify";

@injectable()
export class ClassDetailsDAO extends AbstractDAO<ClassDetailsEntity> {

    public findBySectionId(sectionId: number): ClassDetailsEntity {
        return this.singleSelect(`
            SELECT
                class_details_id,
                section_id,
                alignment,
                hit_die
            FROM
                class_details 
            WHERE
                section_id = :sectionId
        `, { ':sectionId': sectionId }
        );
    }

}

export interface ClassDetailsEntity {
    class_details_id: number;
    section_id: number;
    alignment: string;
    hit_die: string;
}