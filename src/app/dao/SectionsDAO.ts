import { AbstractDAO } from "./AbstractDAO";
import { injectable } from "inversify";

@injectable()
export class SectionsDAO extends AbstractDAO<SectionsEntity> {

    public findByType(type: string): Array<SectionsEntity> {
        return this.select(`
            SELECT
                sections.section_id,
                sections.type,
                sections.subtype,
                sections.lft,
                sections.rgt,
                sections.parent_id,
                sections.name,
                sections.abbrev,
                sections.source,
                sections.description,
                sections.body,
                sections.image,
                sections.alt,
                sections.url,
                sections.create_index
            FROM
                section_index
            JOIN
                sections
            ON
                section_index.section_id = sections.section_id
            WHERE
                section_index.type = :type
        `, { ':type': type }
        );
    }

    public findByTypeAndSubtype(type: string, subype: string): Array<SectionsEntity> {
        return this.select(`
            SELECT
                sections.section_id,
                sections.type,
                sections.subtype,
                sections.lft,
                sections.rgt,
                sections.parent_id,
                sections.name,
                sections.abbrev,
                sections.source,
                sections.description,
                sections.body,
                sections.image,
                sections.alt,
                sections.url,
                sections.create_index
            FROM
                sections
            WHERE
                sections.type = :type AND
                sections.subtype = :subtype
        `, { ':type': type, ':subtype': subype }
        );
    }

    public findByTypeAndSubtypeAndName(type: string, subype: string, name: string): SectionsEntity {
        return this.singleSelect(`
            SELECT
                sections.section_id,
                sections.type,
                sections.subtype,
                sections.lft,
                sections.rgt,
                sections.parent_id,
                sections.name,
                sections.abbrev,
                sections.source,
                sections.description,
                sections.body,
                sections.image,
                sections.alt,
                sections.url,
                sections.create_index
            FROM
                sections
            WHERE
                sections.type = :type AND
                sections.subtype = :subtype AND
                sections.name = :name
        `, { ':type': type, ':subtype': subype, ':name': name }
        );
    }

    public findByParentId(parentId: number): Array<SectionsEntity> {
        return this.select(`
            SELECT
                sections.section_id,
                sections.type,
                sections.subtype,
                sections.lft,
                sections.rgt,
                sections.parent_id,
                sections.name,
                sections.abbrev,
                sections.source,
                sections.description,
                sections.body,
                sections.image,
                sections.alt,
                sections.url,
                sections.create_index
            FROM
                sections
            WHERE
                sections.parent_id = :parentId
        `, { ':parentId': parentId }
        );
    }

}

export interface SectionsEntity {
    section_id: number;
    type: string;
    subtype: string;
    lft: number;
    rgt: number;
    parent_id: number;
    name: string;
    abbrev: string;
    source: string;
    description: string;
    body: string;
    image: string;
    alt: string;
    url: string;
    create_index: boolean;
}
