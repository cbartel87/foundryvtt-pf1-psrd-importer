import { AbstractDAO } from "./AbstractDAO";
import { injectable } from "inversify";

@injectable()
export class LinkDetailsDAO extends AbstractDAO<LinkDetailsEntity> {


    public findBySectionId(sectionId: number): LinkDetailsEntity {
        return this.singleSelect(`
            SELECT
                link_detail_id,
                section_id,
                url,
                display
            FROM
                link_details   
            WHERE
                section_id = :sectionId
        `, { ':sectionId': sectionId }
        );
    }

}

export interface LinkDetailsEntity {
    link_detail_id: number;
    section_id: number;
    url: string;
    display: boolean;
}