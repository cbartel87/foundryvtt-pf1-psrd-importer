import { AbstractDAO } from "./AbstractDAO";
import { injectable } from "inversify";

@injectable()
export class KingdomResourceDetailsDAO extends AbstractDAO<KingdomResourceDetailsEntity> {

}

export interface KingdomResourceDetailsEntity {
    kingdom_resource_detail_id: number;
    section_id: number;
    np: string;
    lot: string;
    kingdom: string;
    discount: string;
    magic_items: string;
    settlement: string;
    special: string;
    resource_limit: string;
    upgrade_from: string;
    upgrade_to: string;
}