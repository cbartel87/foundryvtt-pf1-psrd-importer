import { AbstractDAO } from "./AbstractDAO";
import { injectable } from "inversify";

@injectable()
export class TrapDetailsDAO extends AbstractDAO<TrapDetailsEntity> {

    public findBySectionId(sectionId: number): TrapDetailsEntity {
        return this.singleSelect(`
            SELECT
                trap_details_id,
                section_id,
                cr,
                trap_type,
                perception,
                disable_device,
                duration,
                effect,
                trigger,
                reset
            FROM
                trap_details 
            WHERE
                section_id = :sectionId
        `, { ':sectionId': sectionId });
    }


}

export interface TrapDetailsEntity {
    trap_details_id: number;
    section_id: number;
    cr: string;
    trap_type: string;
    perception: string;
    disable_device: string;
    duration: string;
    effect: string;
    trigger: string;
    reset: string;
}