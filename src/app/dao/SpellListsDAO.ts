import { AbstractDAO } from "./AbstractDAO";
import { injectable } from "inversify";

@injectable()
export class SpellListsDAO extends AbstractDAO<SpellListsEntity> {

    public findBySectionId(sectionId: number): Array<SpellListsEntity> {
        return this.select(`
            SELECT
                spell_list_id,
                section_id,
                level,
                class,
                magic_type
            FROM
                spell_lists       
            WHERE
                section_id = :sectionId
        `, { ':sectionId': sectionId }
        );
    }

}

export interface SpellListsEntity {
    spell_list_id: number;
    section_id: number;
    level: number;
    class: string;
    magic_type: string;
}