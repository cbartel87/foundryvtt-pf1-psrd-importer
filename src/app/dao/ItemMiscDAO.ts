import { AbstractDAO } from "./AbstractDAO";
import { injectable } from "inversify";

@injectable()
export class ItemMiscDAO extends AbstractDAO<ItemMiscEntity> {

    public findBySectionId(sectionId: number): Array<ItemMiscEntity> {
        return this.select(`
            SELECT
                item_misc_id,
                section_id,
                field,
                subsection,
                value
            FROM
                item_misc   
            WHERE
                section_id = :sectionId
        `, { ':sectionId': sectionId }
        );
    }

}

export interface ItemMiscEntity {
    item_misc_id: number;
    section_id: number;
    field: string;
    subsection: string;
    value: string;
}