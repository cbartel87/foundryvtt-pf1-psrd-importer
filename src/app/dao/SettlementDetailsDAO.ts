import { AbstractDAO } from "./AbstractDAO";
import { injectable } from "inversify";


@injectable()
export class SettlementDetailsDAO extends AbstractDAO<SettlementDetailsEntity>{

}

export interface SettlementDetailsEntity {
    settlement_details_id: number;
    section_id: number;
    alignment: string;
    settlement_type: string;
    size: string;
    corruption: string;
    crime: string;
    economy: string;
    law: string;
    lore: string;
    society: string;
    qualities: string;
    danger: string;
    disadvantages: string;
    government: string;
    population: string;
    base_value: string;
    purchase_limit: string;
    spellcasting: string;
    minor_items: string;
    medium_items: string;
    major_items: string;
}