import { AbstractDAO } from "./AbstractDAO";
import { injectable } from "inversify";

@injectable()
export class HauntDetailsDAO extends AbstractDAO<HauntDetailsEntity> {

}

export interface HauntDetailsEntity {
    haunt_details_id: number;
    section_id: number;
    cr: string;
    xp: string;
    haunt_type: string;
    notice: string;
    area: string;
    hp: string;
    destruction: string;
    alignment: string;
    caster_level: string;
    effect: string;
    trigger: string;
    reset: string;
}