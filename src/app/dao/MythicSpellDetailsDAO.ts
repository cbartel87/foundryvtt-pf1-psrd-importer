import { AbstractDAO } from "./AbstractDAO";
import { injectable } from "inversify";

@injectable()
export class MythicSpellDetailsDAO extends AbstractDAO<MythicSpellDetailsEntity> {

}

export interface MythicSpellDetailsEntity {
    spell_detail_id: number;
    section_id: number;
    spell_source: string;
}