import { AbstractDAO } from "./AbstractDAO";
import { injectable } from "inversify";

@injectable()
export class CreatureSpellsDAO extends AbstractDAO<CreatureSpellsEntity> {

    public findBySectionId(sectionId: number): Array<CreatureSpellsEntity> {
        return this.select(`
            SELECT
                creature_spells_id,
                section_id,
                name,
                body
            FROM
                creature_spells;
            WHERE
                section_id = :sectionId
        `, { ':sectionId': sectionId }
        );
    }

}

export interface CreatureSpellsEntity {
    creature_spells_id: number;
    section_id: number;
    name: string;
    body: string;
}