import { AbstractDAO } from "./AbstractDAO";
import { injectable } from "inversify";

@injectable()
export class SpellDescriptorsDAO extends AbstractDAO<SpellDescriptorsEntity> {

    public findBySectionId(sectionId: number): Array<SpellDescriptorsEntity> {
        return this.select(`
            SELECT
                spell_descriptor_id,
                section_id,
                descriptor
            FROM
                spell_descriptors     
            WHERE
                section_id = :sectionId
        `, { ':sectionId': sectionId }
        );
    }

}

export interface SpellDescriptorsEntity {
    spell_descriptor_id: number;
    section_id: number;
    descriptor: string;
}