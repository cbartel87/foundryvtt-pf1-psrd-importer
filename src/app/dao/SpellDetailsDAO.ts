import { AbstractDAO } from "./AbstractDAO";
import { injectable } from "inversify";

@injectable()
export class SpellDetailsDAO extends AbstractDAO<SpellDetailsEntity> {

    public findBySectionId(sectionId: number): SpellDetailsEntity {
        return this.singleSelect(`
            SELECT
                spell_detail_id,
                section_id,
                school,
                subschool_text,
                descriptor_text,
                level_text,
                component_text,
                casting_time,
                preparation_time,
                range,
                duration,
                saving_throw,
                spell_resistance,
                as_spell_id
            FROM
                spell_details
            WHERE
                section_id = :sectionId
        `, { ':sectionId': sectionId });
    }

}

export interface SpellDetailsEntity {
    spell_detail_id: number;
    section_id: number;
    school: string;
    subschool_text: string;
    descriptor_text: string;
    level_text: string;
    component_text: string;
    casting_time: string;
    preparation_time: string;
    range: string;
    duration: string;
    saving_throw: string;
    spell_resistance: string;
    as_spell_id: number;
}