import { AbstractDAO } from "./AbstractDAO";
import { injectable } from "inversify";

@injectable()
export class SpellEffectsDAO extends AbstractDAO<SpellEffectsEntity> {

    public findBySectionId(sectionId: number): Array<SpellEffectsEntity> {
        return this.select(`
            SELECT
                spell_effect_id,
                section_id,
                name,
                description
            FROM
                spell_effects      
            WHERE
                section_id = :sectionId
        `, { ':sectionId': sectionId }
        );
    }

}

export interface SpellEffectsEntity {
    spell_effect_id: number;
    section_id: number;
    name: string;
    description: string;
}