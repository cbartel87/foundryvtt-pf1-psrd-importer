import { AbstractDAO } from "./AbstractDAO";
import { injectable } from "inversify";

@injectable()
export class ResourceDetailsDAO extends AbstractDAO<ResourceDetailsEntity> {

}

export interface ResourceDetailsEntity {
    resource_detail_id: number;
    section_id: number;
    benefit: string;
    resource_create: string;
    earnings: string;
    rooms: string;
    size: string;
    skills: string;
    teams: string;
    time: string;
    upgrade_from: string;
    upgrade_to: string;
    wage: string;
}