import { Database, Statement } from "sql-wasm";
import { inject, injectable } from "inversify";
import { DatabaseProvider } from "../provider/DatabaseProvider";

@injectable()
export class AbstractDAO<T> {


    constructor(@inject(DatabaseProvider) private dbProvider: Provider<Database>) {
    }

    protected getDB(): Database {
        return this.dbProvider.get();
    }

    protected select(statement: string, parameter: object = {}): Array<T> {
        let stmt: Statement = this.getDB().prepare(statement);
        stmt.bind(parameter);
        let result = new Array();
        while (stmt.step()) {
            result.push(stmt.getAsObject())
        }
        return result;
    }

    protected singleSelect(statement: string, parameter: object = {}): T {
        let result = this.select(statement, parameter);
        if (result.length > 0) {
            return result[0];
        }
        return null;
    }

}