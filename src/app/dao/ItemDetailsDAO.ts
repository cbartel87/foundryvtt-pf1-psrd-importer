import { AbstractDAO } from "./AbstractDAO";
import { injectable } from "inversify";

@injectable()
export class ItemDetailsDAO extends AbstractDAO<ItemDetailsEntity> {

    public findBySectionId(sectionId: number): ItemDetailsEntity {
        return this.singleSelect(`
            SELECT
                item_details_id,
                section_id,
                aura,
                slot,
                cl,
                price,
                weight
            FROM
                item_details  
            WHERE
                section_id = :sectionId
        `, { ':sectionId': sectionId }
        );
    }

}

export interface ItemDetailsEntity {
    item_details_id: number;
    section_id: number;
    aura: string;
    slot: string;
    cl: string;
    price: string;
    weight: string;
}