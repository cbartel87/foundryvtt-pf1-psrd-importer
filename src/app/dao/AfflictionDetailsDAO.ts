import { AbstractDAO } from "./AbstractDAO";
import { injectable } from "inversify";

@injectable()
export class AfflictionDetailsDAO extends AbstractDAO<AfflictionDetailsEntity> {

    public findBySectionId(sectionId: number): AfflictionDetailsEntity {
        return this.singleSelect(`
            SELECT
                affliction_details_id,
                section_id,
                contracted,
                save,
                onset,
                frequency,
                effect,
                initial_effect,
                secondary_effect,
                cure,
                cost
            FROM
                affliction_details 
            WHERE
                section_id = :sectionId
        `, { ':sectionId': sectionId }
        );
    }

}

export interface AfflictionDetailsEntity {
    affliction_details_id: number;
    section_id: number;
    contracted: string;
    save: string;
    onset: string;
    frequency: string;
    effect: string;
    initial_effect: string;
    secondary_effect: string;
    cure: string;
    cost: string;
}