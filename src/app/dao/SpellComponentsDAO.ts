import { AbstractDAO } from "./AbstractDAO";
import { injectable } from "inversify";

@injectable()
export class SpellComponentsDAO extends AbstractDAO<SpellComponentsEntity> {

    public findBySectionId(sectionId: number): Array<SpellComponentsEntity> {
        return this.select(`
            SELECT
                spell_component_id,
                section_id,
                component_type,
                description,
                notable
            FROM
                spell_components    
            WHERE
                section_id = :sectionId
        `, { ':sectionId': sectionId }
        );
    }

}

export interface SpellComponentsEntity {
    spell_component_id: number;
    section_id: number;
    component_type: string;
    description: string;
    notable: number;
}