import { AbstractDAO } from "./AbstractDAO";
import { injectable } from "inversify";

@injectable()
export class AnimalCompanionDetailsDAO extends AbstractDAO<AnimalCompanionDetailsEntity> {

    public findBySectionId(sectionId: number): AnimalCompanionDetailsEntity {
        return this.singleSelect(`
            SELECT
                animal_companion_details_id,
                section_id,
                ac,
                attack,
                cmd,
                ability_scores,
                special_qualities,
                special_attacks,
                size,
                speed,
                level
            FROM
                animal_companion_details 
            WHERE
                section_id = :sectionId
        `, { ':sectionId': sectionId }
        );
    }


}

export interface AnimalCompanionDetailsEntity {
    animal_companion_details_id: number;
    section_id: number;
    ac: string;
    attack: string;
    cmd: string;
    ability_scores: string;
    special_qualities: string;
    special_attacks: string;
    size: string;
    speed: string;
    level: string;
}