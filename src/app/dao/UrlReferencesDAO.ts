import { AbstractDAO } from "./AbstractDAO";
import { injectable } from "inversify";

@injectable()
export class UrlReferencesDAO extends AbstractDAO<UrlReferencesEntity> {

    public findBySectionId(sectionId: number): UrlReferencesEntity {
        return this.singleSelect(`
            SELECT
                url_reference_id,
                section_id,
                url
            FROM
                url_references  
            WHERE
                section_id = :sectionId
        `, { ':sectionId': sectionId });
    }

}

export interface UrlReferencesEntity {
    url_reference_id: number;
    section_id: number;
    url: string;
}