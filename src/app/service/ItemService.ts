import { injectable, inject } from "inversify";
import { Creature } from "../model/Creature";
import { Item } from "../../foundry/Item";
import { SettingsProvider } from "../provider/SettingsProvider";

@injectable()
export class ItemService {

    @inject(SettingsProvider)
    private settingsProvider: SettingsProvider;

    public async getItemsForCreature(creature: Creature): Promise<ItemResult> {
        if (!creature.gear) {
            return { itemsNotFound: new Array(), items: new Array() }
        }
        let itemNames = creature.gear.split(",").map(s => s.trim().toLowerCase());
        let items = new Array<Item<any>>();
        for (let pack of game.packs) {
            if (pack.metadata.name == this.settingsProvider.get().itemsPack) {
                let content = await pack.getContent();
                for (let item of content) {
                    let index = itemNames.indexOf(item.data.name.toLowerCase());
                    if (index > -1) {
                        itemNames.splice(index, 1);
                        items.push(item);
                    }
                }
            }
        }

        return {
            itemsNotFound: itemNames,
            items: items
        };
    }

}

export interface ItemResult {
    itemsNotFound: Array<string>;
    items: Array<Item<any>>
}