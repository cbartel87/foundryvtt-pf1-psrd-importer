import { injectable, inject } from "inversify";
import { Creature } from "../model/Creature";
import { Feat } from "../../foundry/Item";
import { SettingsProvider } from "../provider/SettingsProvider";

@injectable()
export class FeatService {

    @inject(SettingsProvider)
    private settingsProvider: SettingsProvider;

    public async getFeatsForCreature(creature: Creature): Promise<FeatResult> {
        let featNames = creature.feats.split(",").map(s => s.trim());
        let feats = new Array<Feat>();
        for (let pack of game.packs) {
            if (pack.metadata.name == this.settingsProvider.get().featsPack) {
                let content = await pack.getContent();
                for (let feat of content) {
                    let index = featNames.indexOf(feat.data.name);
                    if (index > -1) {
                        featNames.splice(index, 1);
                        feats.push(feat);
                    }
                }
            }
        }
        //If there are unmapped feats left, they might be specializations
        for (let featName of featNames) {
            let match = featName.match(/(.* )\(.*\)/);
            console.log(match);
            if (match != null) {
                for (let pack of game.packs) {
                    if (pack.metadata.name == this.settingsProvider.get().featsPack) {
                        let content = await pack.getContent();
                        for (let feat of content) {
                            if (match[1].trim() == feat.data.name) {
                                let index = featNames.indexOf(featName);
                                featNames.splice(index, 1);
                                feat.data.name = featName;
                                feats.push(feat);
                            }
                        }
                    }
                }
            }
        }

        return {
            featsNotFound: featNames,
            feats: feats
        };
    }

}

export interface FeatResult {
    featsNotFound: Array<string>;
    feats: Array<Feat>
}