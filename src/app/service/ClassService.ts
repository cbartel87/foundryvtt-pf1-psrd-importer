import { injectable, inject } from "inversify";
import { SectionRepository } from "../repository/SectionRepository";
import { FoundryService } from "../../foundry/FoundryService";
import { Section } from "../model/Section";
import { Class } from "../../foundry/Item";
import { ClassMapper } from "../mapper/ClassMapper";
import { Creature } from "../model/Creature";
import { SettingsProvider } from "../provider/SettingsProvider";

@injectable()
export class ClassService {

    @inject(SectionRepository)
    private sectionRepository: SectionRepository;

    @inject(FoundryService)
    private foundryService: FoundryService;

    @inject(ClassMapper)
    private classMapper: ClassMapper;

    @inject(SettingsProvider)
    private settingsProvider: SettingsProvider;


    public getCreatureType(name: string): Section {
        return this.sectionRepository.getSectionBySubtypeAndName("section", "creature_type", name);
    }

    public async getRacialHDForCreature(creature: Creature): Promise<Class> {
        for (var pack of game.packs) {
            if (pack.metadata.name == this.settingsProvider.get().racialhdPack) {
                let content = await pack.getContent();
                for (var item of content) {
                    if (item.data.name == creature.creatureType) {
                        return this.classMapper.mapRacialHD(creature, this.getCreatureType(creature.creatureType), item);
                    }
                }
            }
        }

        return null;
    }

    public async getClassForCreature(creature: Creature): Promise<Class> {
        let creatureClass = creature.level.match(/(.*) \d+/)[1];
        for (var pack of game.packs) {
            if (pack.metadata.name == this.settingsProvider.get().classesPack) {
                let content = await pack.getContent();
                for (var item of content) {
                    if (item.data.name.toLowerCase() == creatureClass) {
                        return this.classMapper.mapCreatureClass(creature, item);
                    }
                }
            }
        }

        return null;
    }

}