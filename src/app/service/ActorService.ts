import { inject, injectable } from "inversify";
import { CreatureRepository } from "../repository/CreatureRepository";
import { Creature } from "../model/Creature";
import { Npc } from "../../foundry/Actor";
import { FoundryService } from "../../foundry/FoundryService";
import { NpcMapper } from "../mapper/NpcMapper";
import { ClassService } from "./ClassService";

@injectable()
export class ActorService {

    @inject(CreatureRepository)
    private creatureRepository: CreatureRepository;

    @inject(FoundryService)
    private foundryService: FoundryService;

    @inject(NpcMapper)
    private npcMapper: NpcMapper;

    public getAllCreatures(): Array<Creature> {
        return this.creatureRepository.getAllCreatures();
    }

    public async createNpcFromCreature(creature: Creature, folder: any): Promise<Npc> {
        let npc: Npc = await this.foundryService.createNewNpc(creature.name, folder);
        this.npcMapper.map(creature, npc);
        await npc.update(npc.data);
        return npc;
    }

}