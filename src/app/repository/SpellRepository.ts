import { injectable, inject } from "inversify";
import { SectionRepository } from "./SectionRepository";
import { SpellDetailsDAO } from "../dao/SpellDetailsDAO";
import { Section } from "../model/Section";
import { Spell } from "../model/Spell";

@injectable()
export class SpellRepository {

    @inject(SectionRepository)
    private sectionRepository: SectionRepository;

    @inject(SpellDetailsDAO)
    private spellDetailsDAO: SpellDetailsDAO;

    public getAllSpells(): Array<Spell> {
        return this.sectionRepository.getAllSections('spell').map(s => this.mapToSpell(s));
    }

    private mapToSpell(section: Section): Spell {
        let spell: Spell = <Spell>section;
        let spellDetails = this.spellDetailsDAO.findBySectionId(spell.sectionId);
        spell.spellDetailId = spellDetails.spell_detail_id;
        spell.school = spellDetails.school;
        spell.subschoolText = spellDetails.subschool_text;
        spell.descriptorText = spellDetails.descriptor_text;
        spell.levelText = spellDetails.level_text;
        spell.componentText = spellDetails.component_text;
        spell.castingTime = spellDetails.casting_time;
        spell.preparationTime = spellDetails.preparation_time;
        spell.range = spellDetails.range;
        spell.duration = spellDetails.duration;
        spell.savingThrow = spellDetails.saving_throw;
        spell.spellResistance = spellDetails.spell_resistance;
        spell.asSpellId = spellDetails.as_spell_id;
        return spell;
    }

}