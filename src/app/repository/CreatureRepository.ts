import { injectable, inject } from "inversify";
import { SectionRepository } from "./SectionRepository";
import { Creature, CreatureSpell } from "../model/Creature";
import { CreatureDetailsDAO, CreatureDetailsEntity } from "../dao/CreatureDetailsDAO";
import { CreatureSpellsDAO, CreatureSpellsEntity } from "../dao/CreatureSpellsDAO";
import { Section } from "../model/Section";

@injectable()
export class CreatureRepository {

    @inject(SectionRepository)
    private sectionRepository: SectionRepository;

    @inject(CreatureDetailsDAO)
    private creatureDetailsDAO: CreatureDetailsDAO;

    @inject(CreatureSpellsDAO)
    private creatureSpellsDAO: CreatureSpellsDAO;

    public getAllCreatures(): Array<Creature> {
        return this.sectionRepository.getAllSections('creature').map(s => this.mapToCreature(s));
    }

    private mapToCreature(section: Section): Creature {
        let creature: Creature = <Creature>section;
        let entity: CreatureDetailsEntity = this.creatureDetailsDAO.findBySectionId(creature.sectionId);

        creature.creatureDetailsId = entity.creature_details_id;
        creature.sectionId = entity.section_id;
        creature.sex = entity.sex;
        creature.superRace = entity.super_race;
        creature.level = entity.level;
        creature.cr = entity.cr;
        creature.mr = entity.mr;
        creature.xp = entity.xp;
        creature.alignment = entity.alignment;
        creature.size = entity.size;
        creature.creatureType = entity.creature_type;
        creature.creatureSubtype = entity.creature_subtype;
        creature.init = entity.init;
        creature.senses = entity.senses;
        creature.aura = entity.aura;
        creature.ac = entity.ac;
        creature.hp = entity.hp;
        creature.fortitude = entity.fortitude;
        creature.reflex = entity.reflex;
        creature.will = entity.will;
        creature.defensiveAbilities = entity.defensive_abilities;
        creature.dr = entity.dr;
        creature.resist = entity.resist;
        creature.immune = entity.immune;
        creature.sr = entity.sr;
        creature.weaknesses = entity.weaknesses;
        creature.speed = entity.speed;
        creature.melee = entity.melee;
        creature.ranged = entity.ranged;
        creature.space = entity.space;
        creature.reach = entity.reach;
        creature.specialAttacks = entity.special_attacks;
        creature.strength = entity.strength;
        creature.dexterity = entity.dexterity;
        creature.constitution = entity.constitution;
        creature.intelligence = entity.intelligence;
        creature.wisdom = entity.wisdom;
        creature.charisma = entity.charisma;
        creature.baseAttack = entity.base_attack;
        creature.cmb = entity.cmb;
        creature.cmd = entity.cmd;
        creature.feats = entity.feats;
        creature.skills = entity.skills;
        creature.racialModifiers = entity.racial_modifiers;
        creature.languages = entity.languages;
        creature.specialQualities = entity.special_qualities;
        creature.gear = entity.gear;
        creature.combatGear = entity.combat_gear;
        creature.otherGear = entity.other_gear;
        creature.boon = entity.boon;
        creature.environment = entity.environment;
        creature.organization = entity.organization;
        creature.treasure = entity.treasure;
        creature.hitDice = entity.hit_dice;
        creature.naturalArmor = entity.natural_armor;
        creature.breathWeapon = entity.breath_weapon;
        creature.spells = this.getSpells(creature.sectionId);

        return creature;
    }

    private getSpells(sectionId: number): Array<CreatureSpell> {
        return this.creatureSpellsDAO.findBySectionId(sectionId).map(e => this.mapToCreatureSpell(e));
    }

    private mapToCreatureSpell(entity: CreatureSpellsEntity): CreatureSpell {
        let creatureSpell: CreatureSpell = new CreatureSpell();
        creatureSpell.creatureSpellsId = entity.creature_spells_id;
        creatureSpell.sectionId = entity.section_id;
        creatureSpell.name = entity.name;
        creatureSpell.body = entity.body;
        return creatureSpell;
    }

}