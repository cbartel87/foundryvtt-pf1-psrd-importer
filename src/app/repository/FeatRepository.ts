import { injectable, inject } from "inversify";
import { Section } from "../model/Section";
import { Feat } from "../model/Feat";
import { FeatTypesDAO } from "../dao/FeatTypesDAO";
import { SectionRepository } from "./SectionRepository";


@injectable()
export class FeatRepository {

    @inject(SectionRepository)
    private sectionRepository: SectionRepository;

    @inject(FeatTypesDAO)
    private featTypesDAO: FeatTypesDAO;

    public getAllFeats(): Array<Feat> {
        return this.sectionRepository.getAllSections('feat').map(section => this.mapToFeat(section));
    }

    private mapToFeat(section: Section): Feat {
        let feat: Feat = <Feat>section;
        feat.featTypes = this.featTypesDAO.findBySectionId(section.sectionId).map(featTypeEntity => featTypeEntity.feat_type);
        return feat;
    }

}