import { injectable, inject } from "inversify";
import { SectionsDAO, SectionsEntity } from "../dao/SectionsDAO";
import { Section } from "../model/Section";

@injectable()
export class SectionRepository {

    @inject(SectionsDAO)
    private sectionsDAO: SectionsDAO;

    public getAllSections(type: string): Array<Section> {
        return this.sectionsDAO.findByType(type).map(e => this.mapToSection(e));
    }

    public getAllSectionsBySubtype(type: string, subtype: string): Array<Section> {
        return this.sectionsDAO.findByTypeAndSubtype(type, subtype).map(e => this.mapToSection(e));
    }

    public getSectionBySubtypeAndName(type: string, subtype: string, name: string): Section {
        return this.mapToSection(this.sectionsDAO.findByTypeAndSubtypeAndName(type, subtype, name));
    }

    private mapToSection(entity: SectionsEntity): Section {
        let section = new Section();
        section.sectionId = entity.section_id;
        section.type = entity.type;
        section.subtype = entity.subtype;
        section.lft = entity.lft;
        section.rgt = entity.rgt;
        section.parentId = entity.parent_id;
        section.name = entity.name;
        section.abbrev = entity.abbrev;
        section.source = entity.source;
        section.description = entity.description;
        section.body = entity.body;
        section.image = entity.image;
        section.alt = entity.alt;
        section.url = entity.url;
        section.createIndex = entity.create_index;

        section.subSections = this.createSubSections(section);

        return section;
    }

    private createSubSections(section: Section): Array<Section> {
        return this.sectionsDAO.findByParentId(section.sectionId).map(sectionEntity => this.mapToSection(sectionEntity));
    }
}