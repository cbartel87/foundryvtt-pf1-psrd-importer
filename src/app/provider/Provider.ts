interface Provider<T> {
    get(): T;
}