import { Settings } from "../Settings";
import { injectable } from "inversify";

@injectable()
export class SettingsProvider implements Provider<Settings> {
    get(): Settings {
        return game.settings.get('pf1-psrd-importer', 'settings');
    }

    set(settings: Settings) {
        game.settings.set('pf1-psrd-importer', 'settings', settings);
    }

}