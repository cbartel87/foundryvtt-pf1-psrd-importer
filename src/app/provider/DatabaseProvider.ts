import { Database, SQLWasm } from "sql-wasm";
import { injectable } from "inversify";
import { DATABASE_LOADED_HOOK } from "../Constants";

@injectable()
export class DatabaseProvider implements Provider<Database>  {

    private db: Database;
    private sqlWasm: SQLWasm;

    loadDatabase(databasePath: string) {
        if (this.db) {
            console.log("PF1-PSRD-Importer | closing previous database.")
            this.db.close();
        }

        console.log("PF1-PSRD-Importer | loading database from path: " + databasePath);
        var xhr = new XMLHttpRequest();
        xhr.open('GET', databasePath, true);
        xhr.responseType = 'arraybuffer';

        xhr.onload = e => {
            var uInt8Array = new Uint8Array(xhr.response);
            this.db = new this.sqlWasm.Database(Array.from(uInt8Array));
            console.log("PF1-PSRD-Importer | database loaded.")
            Hooks.call(DATABASE_LOADED_HOOK, databasePath);
        };
        xhr.send();
    }

    setSqlWasm(sqlWasm: SQLWasm) {
        this.sqlWasm = sqlWasm;
    }

    get(): Database {
        return this.db;
    }

}