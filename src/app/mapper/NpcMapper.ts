import { injectable } from "inversify";
import { Creature } from "../model/Creature";
import { Npc } from "../../foundry/Actor";
import { NpcTemplate } from "../../foundry/NpcTemplate";
import { Section } from "../model/Section";

@injectable()
export class NpcMapper {

    public map(creature: Creature, npc: Npc) {

        let data: NpcTemplate = npc.data.data;

        //abilities
        data.abilities.str.value = +creature.strength;
        data.abilities.dex.value = +creature.dexterity;
        data.abilities.con.value = +creature.constitution;
        data.abilities.int.value = +creature.intelligence;
        data.abilities.wis.value = +creature.wisdom;
        data.abilities.cha.value = +creature.charisma;

        //attributes
        data.attributes.ac.flatFooted.total = +creature.ac.match(/flat-footed (\d+)/)[1];
        data.attributes.ac.normal.total = +creature.ac.match(/(\d+)/)[1];
        data.attributes.ac.touch.total = +creature.ac.match(/touch (\d+)/)[1];
        data.attributes.acNotes = creature.ac;
        if (creature.defensiveAbilities) {
            data.attributes.acNotes += "\n" + creature.defensiveAbilities;
        }
        // data.attributes.acp= 
        // data.attributes.attack.general=
        // data.attributes.attack.melee=
        // data.attributes.attack.ranged=
        // data.attributes.bab.value=
        data.attributes.cmb.value = +creature.cmb.match(/(\+?-?\d+)/)[1];
        if (creature.specialAttacks) {
            data.attributes.cmbNotes = creature.specialAttacks;
        }
        data.attributes.cmbNotes = creature.cmb;
        data.attributes.cmd.value = +creature.cmd.match(/(\+?-?\d+)/)[1];
        data.attributes.cmdNotes = creature.cmd;
        // data.attributes.creatureType=
        // data.attributes.damage=
        // data.attributes.encumbrance=
        // data.attributes.energyDrain=
        // data.attributes.hd.total = +creature.hp.match(/\((\d+)d/)[1];
        // data.attributes.hp.value = +creature.hp.match(/(\d+)/)[1];
        // data.attributes.hp.max = data.attributes.hp.value;
        data.attributes.init.total = +creature.init;
        // data.attributes.maxDexBonus=
        let naturalACMatch = creature.ac.match(/(\d+) natural/);
        if (naturalACMatch) {
            data.attributes.naturalAC = +naturalACMatch[1];
        }
        // data.attributes.prof=
        // data.attributes.quadruped=
        // data.attributes.saveNotes=
        // data.attributes.savingThrows.fort.total = +creature.fortitude;
        // data.attributes.savingThrows.ref.total = +creature.reflex;
        // data.attributes.savingThrows.will.total = +creature.will;
        // data.attributes.speed.burrow=
        // data.attributes.speed.climb=
        // data.attributes.speed.fly=
        // data.attributes.speed.land=
        // data.attributes.speed.swim=
        // data.attributes.spellLevel=
        // data.attributes.spells=
        data.attributes.sr.total = +creature.sr;



        data.traits.senses = creature.senses;
        data.traits.dr = creature.dr;
        data.traits.eres = creature.resist;
        data.traits.di.custom = creature.immune;
        data.traits.dv.custom = creature.weaknesses;
        //TODO data.traits.size = creature.size;
        data.traits.languages.custom = creature.languages;

        data.details.cr = this.getCR(creature.cr);

        data.details.type = creature.creatureType;
        if (creature.creatureSubtype) {
            data.details.type += ` (${creature.creatureSubtype})`;
        }
        data.details.alignment = creature.alignment;
        data.details.source = creature.source;
        data.details.notes.value = this.createNotes(creature);
        if (creature.organization) {
            data.details.notes.value += `<br /><h2>Organization</h2><p>${creature.organization}</p>`
        }
        if (creature.treasure) {
            data.details.notes.value += `<br /><h2>Treasure</h2><p>${creature.treasure}</p>`
        }

    }

    createNotes(section: Section, text = ""): string {
        text += `<h2>${section.name}</h2>`
        if (section.description) {
            text += `<p>${section.description}</p>`
        }
        if (section.body) {
            text += `<p>${section.body}</p>`
        }
        text += section.subSections.forEach(section => this.createNotes(section, text));
        return text;
    }

    private getCR(value: string): string {
        let split = value.split("/");
        var result: number = +split[0];
        if (split.length > 1) {
            result /= +split[1];
        }
        return result.toString();
    }

}