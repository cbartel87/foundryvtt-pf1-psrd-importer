import { injectable } from "inversify";
import { Class } from "../../foundry/Item";
import { Section } from "../model/Section";
import { Creature } from "../model/Creature";

@injectable()
export class ClassMapper {

    public mapRacialHD(creature: Creature, creatureType: Section, clazz: Class): Class {
        let data = clazz.data.data;

        //Level
        data.levels = +creature.hp.match(/\((\d+)d/)[1];

        //HitPoints
        let totalHP = +creature.hp.match(/(\d+)/)[1];
        let bonusHP = +creature.hp.match(/(\+\d+)/)[1];
        data.hp = totalHP - bonusHP;

        //Class Name
        if (creature.creatureSubtype != null) {
            clazz.data.name += ` (${creature.creatureSubtype})`;
        }

        return clazz;
    }

    public mapCreatureClass(creature: Creature, clazz: Class): Class {
        let data = clazz.data.data;

        data.levels = +creature.level.match(/.* (\d+)/)[1];

        return clazz;
    }
}