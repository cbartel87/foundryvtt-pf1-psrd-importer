import { Section } from "./Section";

export class Creature extends Section {
    creatureDetailsId: number;
    sectionId: number;
    sex: string;
    superRace: string;
    level: string;
    cr: string;
    mr: string;
    xp: string;
    alignment: string;
    size: string;
    creatureType: string;
    creatureSubtype: string;
    init: string;
    senses: string;
    aura: string;
    ac: string;
    hp: string;
    fortitude: string;
    reflex: string;
    will: string;
    defensiveAbilities: string;
    dr: string;
    resist: string;
    immune: string;
    sr: string;
    weaknesses: string;
    speed: string;
    melee: string;
    ranged: string;
    space: string;
    reach: string;
    specialAttacks: string;
    strength: string;
    dexterity: string;
    constitution: string;
    intelligence: string;
    wisdom: string;
    charisma: string;
    baseAttack: string;
    cmb: string;
    cmd: string;
    feats: string;
    skills: string;
    racialModifiers: string;
    languages: string;
    specialQualities: string;
    gear: string;
    combatGear: string;
    otherGear: string;
    boon: string;
    environment: string;
    organization: string;
    treasure: string;
    hitDice: string;
    naturalArmor: string;
    breathWeapon: string;
    spells: Array<CreatureSpell>;
}

export class CreatureSpell {
    creatureSpellsId: number;
    sectionId: number;
    name: string;
    body: string;
}