import { Section } from "./Section";

export class Spell extends Section {
    spellDetailId: number;
    school: string;
    subschoolText: string;
    descriptorText: string;
    levelText: string;
    componentText: string;
    castingTime: string;
    preparationTime: string;
    range: string;
    duration: string;
    savingThrow: string;
    spellResistance: string;
    asSpellId: number;
}

export class SpellDetails {
    
}