export class Section {
    sectionId: number;
    type: string;
    subtype: string;
    lft: number;
    rgt: number;
    parentId: number;
    name: string;
    abbrev: string;
    source: string;
    description: string;
    body: string;
    image: string;
    alt: string;
    url: string;
    createIndex: boolean;
    subSections: Array<Section>;
}