export class Settings {

    databasePath: string;
    classesPack: string;
    racialhdPack: string;
    featsPack: string;
    itemsPack: string;


    static default(): Settings {
        let settings = new Settings();
        settings.databasePath = null;
        settings.classesPack = "classes";
        settings.racialhdPack = "racialhd";
        settings.featsPack = "feats";
        settings.itemsPack = "items";
        return settings;
    }

}