const path = require('path');

module.exports = {
    entry: path.join(__dirname, 'src/main.ts'),
    output: {
        filename: 'main.js',
        sourceMapFilename: 'main.js.map',
        path: path.join(__dirname, 'scripts')
    },
    devtool: 'source-map',
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                loader: 'ts-loader',
                exclude: /node_modules/,
            },
        ]
    },
    resolve: {
        extensions: [".tsx", ".ts", ".js"]
    },
    externals: {
        "fs": "commonjs fs"
    },
    mode: 'development'
};