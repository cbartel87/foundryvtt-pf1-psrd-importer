# foundryvtt-pf1-psrd-importer

## Intro

### Download
https://gitlab.com/cbartel87/foundryvtt-pf1-psrd-importer/-/raw/master/module.json

### How To Use

Navigate to the Game Settings Tab to use the PSRD Importer functions.
First, you need to setup the module:
1. Upload a .db file from devonjones psrd data (https://github.com/devonjones/PSRD-Data) to your foundry server
2. Go to Settings, and select the file
3. Choose your compendium packs to tell the importer which data it should use


## Documentation

See the [Wiki Pages](../../wikis/home)




